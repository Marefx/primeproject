package rsp.MainPackage;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import rsp.MainPackage.Models.Account;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Run {
    public Account rsAccount;

    public static void main(String[] args) {
        System.out.println("Enter number of accounts to make: ");
        Scanner sc = new Scanner(System.in);
        Helpers.NUMBER_OF_ACCOUNTS = sc.nextInt();
        System.out.println("Attempting to create " + Helpers.NUMBER_OF_ACCOUNTS + " accounts");
        Helpers.sleeping(1000);

        try {
            new Run().begin();
        } catch (IOException ex) {
            // Napaka pri komunikaciji z account strežnikom - ne morem delati brez accjev
            Logger.getLogger(Run.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void begin() throws IOException {
        if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
            System.out.println("Running on Linux!");
            System.setProperty("webdriver.chrome.driver", "/home/administrator/Desktop/chromedriver");
        } else {
            System.out.println("Running on Windows!");
            System.setProperty("webdriver.gecko.driver", "c:\\Gecko\\geckodriver.exe");
            System.setProperty("webdriver.chrome.driver", "c:\\Gecko\\chromedriver.exe");
        }

        // Add the WebDriver proxy capability.
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("localhost:8118");
        proxy.setFtpProxy("localhost:8118");
        proxy.setSslProxy("localhost:8118");
        // proxy.setSocksProxy("localhost:8118");

        ChromeOptions options = new ChromeOptions();
        // options.setCapability("proxy", proxy);
        options.setAcceptInsecureCerts(true);
        options.addArguments("start-maximized");
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--no-sandbox");


        for (int count = 0; count < Helpers.NUMBER_OF_ACCOUNTS; count++) {
            //FirefoxDriver driver = new FirefoxDriver();
            WebDriver driver = new ChromeDriver(options);
            // Generate account details
            Account amazonAccount = new Account();
            Account twitchAccount = amazonAccount;

            // 1. pridobit kreditne kartice
            if (Elfqrin.getCreditCards(driver)) {
                Helpers.sleeping(750);
            }

            Amazon.FINISHED = false;
            // 2. registracija amazon računa
            if (Amazon.createAmazonAccount(driver, amazonAccount.username, amazonAccount.password, amazonAccount.email)) {
                Helpers.sleeping(750);
            }
            // 3. dodat kartico, addresso na amazon račun in dobit amazon prime
            if (Amazon.applyAmazonPrime(driver, amazonAccount.password, amazonAccount.email)) {
                Helpers.sleeping(750);
            }
            // 4. registracija twitch računa
            try {
                if (Twitch.twitchRegister(driver, twitchAccount)) {
                    StringBuilder urlEncodedString = new StringBuilder();
                    urlEncodedString.append("type=").append("twitch");
                    urlEncodedString.append("&name=").append(twitchAccount.username);
                    urlEncodedString.append("&password=").append(twitchAccount.password);

                    Helpers.executePost(urlEncodedString.toString());
                    Helpers.sleeping(2000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 5. povezat twitch in amazon račun
            // 6. pridobit twitch prime
            // 7. pridobit runescape račun od API
            // 8. linkat twitch in runescape račun

            // SIMULACIJA
            try {
                Helpers.ACCOUNTS_LINKED = linkAccounts(driver, amazonAccount);

                // AŽURIRANJE STATUSOV
                if (!Helpers.DEBUG_MODE) {
                    Helpers.ACCOUNTS_LINKED = false;
                    Amazon.FINISHED = false;
                    Helpers.setStatusFinished(twitchAccount, amazonAccount, rsAccount);
                    // throw new IOException("TODO - ažuriranje statusa uporabnikov");
                }
            } catch (Exception ex) {
                Helpers.setStatusFailed(twitchAccount, amazonAccount, rsAccount);
                Logger.getLogger(Run.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (Helpers.NUMBER_OF_ACCOUNTS != 1) {
                    Helpers.sleeping(1000);
                    driver.quit();
                }
            }
        }
    }

    public boolean linkAccounts(WebDriver driver, Account AmazonAccount) throws IOException {
        /********************
         ****** TWITCH ******
         ********************/
        driver.get(Helpers.TWITCH_URL);
        Helpers.sleeping(2000);

        WebElement TryPrime = Helpers.waitForElement(driver, By.xpath("/html/body/div[1]/div/div[2]/nav/div/div[1]/div[1]/div/a[1]"), 20);
        TryPrime.click();

        Helpers.sleeping(3000);// da se prikaže okno

        Iterator<String> windowsIterator = driver.getWindowHandles().iterator();
        String window1 = windowsIterator.next();
        String window2 = windowsIterator.next();

        driver.switchTo().window(window2);
        Helpers.sleeping(15000);

        WebElement ResidenceCountry = driver.findElement(By.xpath("//*[@id=\"a-page\"]/div[1]/div[3]/div[15]/div/div/div[1]"));
        ResidenceCountry.click();
        Helpers.sleeping(250);
        ResidenceCountry.click();
        Helpers.sleeping(250);

        /********************
         ****** AMAZON ******
         ********************/
        System.out.println(AmazonAccount.toString());
        WebElement confirm = Helpers.waitForElement(driver, By.xpath("/html/body/div[2]/div/div[2]/div/form/input[1]"), 20);
        confirm.click();
        // Rabmo mal dlje časa da aktivira twitch prime...
        Helpers.sleeping(15000);
        // TODO: če v roku 8s ne aktivira => quit

        /********************
         ****** TWITCH ******
         ********************/
        driver.switchTo().window(window1);
        driver.navigate().refresh();

        WebElement seeLoot = Helpers.waitForElement(driver, By.xpath("/html/body/div[1]/div/div[2]/nav/div/div[3]/div/div/div[1]/div/div[1]/button"), 20);
        seeLoot.click();
        Helpers.sleeping(2500);

        WebElement rsContainer = Helpers.waitForElement(driver, By.xpath("//div[contains(@data-a-target, '0eb24851-53a7-7f1b-d830-b2f93514b955')]/parent::*"), 20);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rsContainer);
        Helpers.sleeping(1000);

        try {
            //kliknit na claim offer prvo
            rsContainer = Helpers.waitForElement(driver, By.xpath("//div[contains(@data-a-target, '0eb24851-53a7-7f1b-d830-b2f93514b955')]/parent::*"), 20);
            WebElement claimOffer = rsContainer.findElement(By.tagName("button"));
            //TODO: get button text and quit if "Start Your Free Trial"
            claimOffer.click();
            Helpers.sleeping(5000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Helpers.sleeping(1500);
            driver.quit();
        }

        //na twitch.tv se pa scrolanju ponovno naloži element in ga moram ponovno poiskat
        rsContainer = Helpers.waitForElement(driver, By.xpath("//div[contains(@data-a-target, '0eb24851-53a7-7f1b-d830-b2f93514b955')]/parent::*"), 20);
        WebElement startTrial = rsContainer.findElement(By.tagName("a"));
        startTrial.click();

        //nevem kaj bi to kliknal
//        WebElement linkTwitchRS = Helpers.waitForElement(driver, By.xpath("//*[@id=\"root\"]/div/div[2]/nav/div/div[3]/div/div/div[2]/div[2]/div/div[2]/div[3]/div/div/div/div[13]/div/div/div[3]/div[2]/div/div/p[1]/a"), 10);
//        linkTwitchRS.click();

        /***********************
         ****** RUNESCAPE ******
         ***********************/

        Helpers.sleeping(3000); //da se window odpre

        Iterator<String> windowIterator = driver.getWindowHandles().iterator();
        windowIterator.next();
        windowIterator.next();
        driver.switchTo().window(windowIterator.next());

        WebElement getLoot = Helpers.waitForElement(driver, By.xpath("/html/body/section[1]/div[2]/a"), 20);
        getLoot.click();

        WebElement authorize = Helpers.waitForElement(driver, By.xpath("//*[@id=\"kraken_auth\"]/div/div/div[6]/form/fieldset/button[1]"), 20);
        authorize.click();

        WebElement rsLogin = Helpers.waitForElement(driver, By.xpath("/html/body/main/div/div/div[2]/div[1]/a"), 20);
        rsLogin.click();

        if (Helpers.DEBUG_MODE) { // za debug uporabim svoja accja, da ne spreminjam statusov po nepotrebnem
            rsAccount = new Account("mrkneegrow9946@marefx.com", "Geslo123");
        } else {
            rsAccount = Helpers.fetchRuneScapeAcc();
        }

        Helpers.sleeping(500);

        WebElement rsUsername = Helpers.waitForElement(driver, By.xpath("//*[@id=\"login-username\"]"), 20);
        rsUsername.sendKeys(rsAccount.username);
        Helpers.sleeping(250);

        WebElement rsPassword = driver.findElement(By.xpath("//*[@id=\"login-password\"]"));
        rsPassword.sendKeys(rsAccount.password);
        Helpers.sleeping(250);

        rsUsername.submit();

        WebElement rsConfirm = Helpers.waitForElement(driver, By.xpath("/html/body/main/div/div/section/div/div/button"), 20);
        rsConfirm.click();

        System.out.println("Accounts successfully linked!");

        return true;
    }
}