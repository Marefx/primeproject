package rsp.MainPackage.Models;

public class CaptchaResponse {
    public boolean OK;
    public String value;
    private String response;

    public CaptchaResponse() {
        OK = false;
        value = "";
        response = "NO RESPONSE";
    }

    public CaptchaResponse(String response) {
        this();

        String[] values = response.split("\\|");

        this.response = response;
        if (values[0].equals("OK") || values.length == 2) {
            OK = true;
            value = values[1];
        }
    }

    @Override
    public String toString() {
        return response;
    }
}