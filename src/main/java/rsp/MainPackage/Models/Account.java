package rsp.MainPackage.Models;

import com.github.javafaker.Faker;
import com.google.gson.JsonElement;

public class Account {
    public String id;
    public String username;
    public String password;
    public String email;

    public Account() {
        Faker faker = new Faker();

        int randomNum = faker.number().numberBetween(100, 999);
        this.username = faker.name().username().toLowerCase().replaceAll("\\.", "") + randomNum;
        this.password = "Geslo123";
        this.email = username + "@gmail.com";
    }

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
        this.email = username + "@gmail.com";
    }

    public Account(String username, String password, String email) {
        this(username, password);
        this.email = email;
    }

    public Account(JsonElement id, JsonElement username, JsonElement password) {
        this(username.toString().replace("\"", ""), password.toString().replace("\"", ""));
        this.id = id.toString().replace("\"", "");
    }

    @Override
    public String toString() {
        return id + " " + username + " " + password + " " + email;
    }
}