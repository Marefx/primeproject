package rsp.MainPackage.Models;

public class CreditCard {
    public String number;
    public String expiryYear;
    public String expiryMonth;

    public CreditCard(String number, String expiryYear, String expiryMonth) {
        this.number = number;
        this.expiryYear = expiryYear;
        this.expiryMonth = expiryMonth;
    }
}
