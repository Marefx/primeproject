package rsp.MainPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import rsp.MainPackage.Models.Account;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Amazon {
    public static boolean FINISHED = false; // resetirat na false ko se acc konca
    private final static String AMAZON_URL = "https://amazon.com";
    private final static String AMAZON_REGISTER_URL = "https://www.amazon.com/your-account";
    private final static String AMAZON_PRIME_URL = "https://www.amazon.com/amazonprime?_encoding=UTF8&ref=nav_menu_greenbg_rocketman_ft_cta";
    private final static String CARD_NAME = "Pede Munn";

    public static boolean createAmazonAccount(WebDriver webDriver, String user, String pass, String mail) {
        webDriver.get(AMAZON_REGISTER_URL);
        // click on sign up
        WebElement signUp = Helpers.waitForElement(webDriver, By.xpath("//div[contains(@data-card-identifier, 'SignInAndSecurity')]"), 20);
        signUp.click();

        WebElement registerButton = Helpers.waitForElement(webDriver, By.id("createAccountSubmit"), 20);
        registerButton.click();

//        WebElement signUp = Helpers.waitForElement(webDriver, By.className("nav-signin-tooltip-footer"), 10).findElement(By.className("nav-a"));
//        signUp.click();

        // apply form values
        WebElement username = Helpers.waitForElement(webDriver, By.id("ap_customer_name"), 20);
        username.sendKeys(user);
        Helpers.sleeping(250);
        WebElement email = webDriver.findElement(By.id("ap_email"));
        email.sendKeys(mail);
        Helpers.sleeping(250);
        WebElement password = webDriver.findElement(By.id("ap_password"));
        password.sendKeys(pass);
        Helpers.sleeping(250);
        WebElement password_check = webDriver.findElement(By.id("ap_password_check"));
        password_check.sendKeys(pass);
        Helpers.sleeping(250);
        // click register
        webDriver.findElement(By.id("continue")).click();
        Helpers.sleeping(250);

        return true;
    }

    public static boolean applyAmazonPrime(WebDriver webDriver, String sendPassword, String sendEmail) {
        webDriver.get(AMAZON_PRIME_URL);

        WebElement trialButton = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@aria-labelledby, 'prime-header-CTA-announce')]"), 20);
        trialButton.click();

        WebElement ccname = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-accountHolderName')]"), 20);
        ccname.sendKeys(CARD_NAME);
        Helpers.sleeping(500);

        if (applyCreditCard(webDriver)) {
            Helpers.sleeping(750);

            // 162 LEONARD ST, NEW YORK, NY, 10013-4350, United States
            String street = ThreadLocalRandom.current().nextInt(100, 999) + " LEONARD ST";
            String city = "NEW YORK";
            String state = "NY";
            String ZIP = "10013-4350";
            String phoneNumber = "654-549-" + ThreadLocalRandom.current().nextInt(1000, 9999);

//            String street = ThreadLocalRandom.current().nextInt(100, 999) + " Farnum Road";
//            String city = "New York";
//            String state = "NY";
//            String ZIP = "10011";
//            String phoneNumber = "646-522-" + ThreadLocalRandom.current().nextInt(1000, 9999);

            WebElement streetField = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-line1')]"), 20);
            streetField.sendKeys(street);
            WebElement countryField = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-city')]"), 20);
            countryField.sendKeys(city);
            WebElement stateField = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-stateOrRegion')]"), 20);
            stateField.sendKeys(state);
            WebElement zipField = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-postalCode')]"), 20);
            zipField.sendKeys(ZIP);
            WebElement phoneNumberField = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-phoneNumber')]"), 20);
            phoneNumberField.sendKeys(phoneNumber);
            Helpers.sleeping(1000);
        }

        for (int i = 0; i < 5; i++) {
            try {
                WebElement addAddressButton = webDriver.findElement(By.xpath("//input[contains(@name, 'ppw-widgetEvent:AddAddressEvent')]"));
                addAddressButton.click();
                Helpers.sleeping(5000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

//        try {
//            WebElement continueButton1 = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-widgetEvent:PreferencePaymentOptionSelectionEvent')]"), 20);
//            continueButton1.click();
//            Helpers.sleeping(1000);
//
//            WebElement addressConfirm1 = webDriver.findElement(By.xpath("//input[contains(@name, 'ppw-widgetEvent:SelectAddressEvent')]"));
//            addressConfirm1.click();
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }

        Helpers.sleeping(5000);
        WebElement submitTrialButton = webDriver.findElement(By.xpath("//input[contains(@aria-labelledby, 'a-autoid-0-announce')]"));
        submitTrialButton.click();
        Helpers.sleeping(750);

        try {
            WebElement trialButtonFinish = Helpers.waitForElement(webDriver, By.xpath("//input[@name='formSubmit' and contains(@aria-labelledby, 'a-autoid-0-announce')]"), 20);
            if (trialButtonFinish.isDisplayed()) {
                System.out.println("Success!");
                StringBuilder urlEncodedString = new StringBuilder();
                urlEncodedString.append("type=").append("amazon");
                urlEncodedString.append("&name=").append(sendEmail);
                urlEncodedString.append("&password=").append(sendPassword);

                if (!FINISHED) {
                    Helpers.executePost(urlEncodedString.toString());
                    FINISHED = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Add card again");

            for (int i = 0; i < 8; i++) {
                try {
                    String currentUrl = webDriver.getCurrentUrl();
                    if (!currentUrl.contains("amazonprime")) {
                        WebElement changeButton = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@value, 'change')]"), 20);
                        if (!changeButton.isDisplayed()) {
                            submitTrialButton = webDriver.findElement(By.xpath("//input[contains(@aria-labelledby, 'a-autoid-0-announce')]"));
                            submitTrialButton.click();
                            Helpers.sleeping(750);
                        } else {
                            changeButton.click();
                        }
                        Helpers.sleeping(1000);
                        WebElement addCardButton = Helpers.waitForElement(webDriver, By.xpath("//span[contains(@class, 'a-size-base')]"), 20);
                        addCardButton.click();
                        Helpers.sleeping(1500);

                        ccname = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-accountHolderName')]"), 20);
                        ccname.sendKeys(CARD_NAME);
                        Helpers.sleeping(500);

                        if (applyCreditCard(webDriver)) {
                            Helpers.sleeping(750);

                            WebElement addressConfirm = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-widgetEvent:SelectAddressEvent')]"), 20);
                            addressConfirm.click();
                            Helpers.sleeping(5000);

                            WebElement continueButton = webDriver.findElement(By.xpath("//input[contains(@name, 'ppw-widgetEvent:PreferencePaymentOptionSelectionEvent')]"));
                            continueButton.click();
                            Helpers.sleeping(5000);

                            WebElement submitTrialButton2 = webDriver.findElement(By.xpath("//input[contains(@aria-labelledby, 'a-autoid-0-announce')]"));
                            submitTrialButton2.click();
                            Helpers.sleeping(3800);

                            new WebDriverWait(webDriver, 15).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//input[contains(@aria-labelledby, 'a-autoid-0-announce')]")));
                        }
                    } else {
                        System.out.println("DONE");
                        StringBuilder urlEncodedString = new StringBuilder();
                        urlEncodedString.append("type=").append("amazon");
                        urlEncodedString.append("&name=").append(sendEmail);
                        urlEncodedString.append("&password=").append(sendPassword);

                        if (!FINISHED) {
                            Helpers.executePost(urlEncodedString.toString());
                            FINISHED = true;
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

        return true;
    }

    private static boolean applyCreditCard(WebDriver webDriver) {
        System.out.println("NUMBER: " + Helpers.creditCards.get(0).number + " MONTH: " + Helpers.creditCards.get(0).expiryMonth + " YEAR: " + Helpers.creditCards.get(0).expiryYear);
        WebElement ccnumber = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'addCreditCardNumber')]"), 20);
        ccnumber.sendKeys(Helpers.creditCards.get(0).number);
        Helpers.sleeping(500);

        String menuSelector = ".a-dropdown-prompt";
        String menuItemSelector = ".a-dropdown-common .a-dropdown-item";

        // month
        List<WebElement> menu = webDriver.findElements(By.cssSelector(menuSelector));
        menu.get(0).click();
        List<WebElement> menuItem = webDriver.findElements(By.cssSelector(menuItemSelector));
        menuItem.get(Integer.parseInt(Helpers.creditCards.get(0).expiryMonth.replaceFirst("^0+(?!$)", "")) - 1).click();
        Helpers.sleeping(750);

        // year
        menu.get(1).click();
        Helpers.sleeping(750);

        List<WebElement> menuItems = webDriver.findElements(By.cssSelector(menuItemSelector));
        int chooseYear = 12;
        String year = Helpers.creditCards.get(0).expiryYear.replaceAll("\\s+", "");
        switch (year) {
            case "2018":
                break;
            case "2019":
                chooseYear += 1;
                break;
            case "2020":
                chooseYear += 2;
                break;
            case "2021":
                chooseYear += 3;
                break;
            case "2022":
                chooseYear += 4;
                break;
            case "2023":
                chooseYear += 5;
                break;
            default:
                break;
        }

        Helpers.sleeping(750);

        menuItems.get(chooseYear).click();
        Helpers.creditCards.remove(0);
        Helpers.sleeping(750);

        WebElement addNewCardButton = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ppw-widgetEvent:AddCreditCardEvent')]"), 20);
        addNewCardButton.click();
        Helpers.sleeping(1500);

        return true;
    }

    public void login(WebDriver driver, Account AmazonAccount) {
        WebElement AmazonEmail = Helpers.waitForElement(driver, By.xpath("//*[@id=\"ap_email\"]"), 20);
        AmazonEmail.sendKeys(AmazonAccount.username);
        Helpers.sleeping(250);

        WebElement AmazonPassword = Helpers.waitForElement(driver, By.xpath("//*[@id=\"ap_password\"]"), 5);
        AmazonPassword.sendKeys(AmazonAccount.password);
        Helpers.sleeping(250);

        WebElement AmazonSignIn = Helpers.waitForElement(driver, By.xpath("//*[@id=\"signInSubmit\"]"), 5);
        AmazonSignIn.click();
    }
}
