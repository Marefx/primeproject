package rsp.MainPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import rsp.MainPackage.Models.CreditCard;

public class Elfqrin {
    private final static String CRED_CARD_URL = "https://www.elfqrin.com/discard_credit_card_generator.php";

    public static boolean getCreditCards(WebDriver webDriver) {
        final String cred_card_pattern = "473702907xxxxxxx";
        webDriver.get(CRED_CARD_URL);
        Helpers.sleeping(750);

        // working card NUMBER: 4737029076536243 MONTH: 11 YEAR:  2020
        // working card NUMBER: 4737029076747048 MONTH: 09 YEAR:  2021
        // working card NUMBER: 4737029075272600 MONTH: 11 YEAR:  2021
        //Helpers.creditCards.add(new CreditCard("4737029076536243", "2020", "11"));
        //Helpers.creditCards.add(new CreditCard("4737029076747048", "2021", "09"));
        //Helpers.creditCards.add(new CreditCard("4737029075272600", "2021", "11"));

        WebElement pattern = Helpers.waitForElement(webDriver, By.xpath("//input[contains(@name, 'ccp')]"), 20);
        pattern.sendKeys(cred_card_pattern);

        WebElement numberOfCards = webDriver.findElement(By.xpath("//input[contains(@name, 'ccghm')]"));
        numberOfCards.clear();
        Helpers.sleeping(250);
        numberOfCards.sendKeys("10");

        WebElement expiryDate = webDriver.findElement(By.xpath("//input[contains(@name, 'ccexpdat')]"));
        expiryDate.click();

        WebElement generate = webDriver.findElement(By.xpath("//input[contains(@value, 'Generate')]"));
        generate.click();
        Helpers.sleeping(250);

        WebElement cardsList = webDriver.findElement(By.xpath("//textarea[contains(@name, 'output2')]"));
        String result = cardsList.getAttribute("value");
        String lines[] = result.split("\\r?\\n");

        for (String line : lines) {
            String number = line.split(",")[0];
            String expiry = line.split(",")[1];
            String year = expiry.split("/")[0];
            String month = expiry.split("/")[1];

            Helpers.creditCards.add(new CreditCard(number, year, month));
        }

        return true;
    }
}
