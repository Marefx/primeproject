package rsp.MainPackage;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import rsp.MainPackage.Models.Account;
import rsp.MainPackage.Models.CreditCard;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Helpers {
    public static int NUMBER_OF_ACCOUNTS = 1;
    public static boolean ACCOUNTS_LINKED = false;
    public final static boolean DEBUG_MODE = false;
    public final static boolean REGISTER_TWITCH = true;
    public final static String ACCOUNT_SERVER_URL = "https://rsbots-api.marefx.com/api/provider/";
    public final static String TWITCH_URL = "https://www.twitch.tv/";
    public static List<CreditCard> creditCards = new ArrayList<>();
    public static ArrayList<String> OpenedWindows = new ArrayList<>();

    public static Account fetchAmazonAcc() throws IOException {
        return fetchAccount(ACCOUNT_SERVER_URL + "account/amazon");
    }

    public static Account fetchTwitchAcc() throws IOException {
        return fetchAccount(ACCOUNT_SERVER_URL + "account/twitch");
    }

    public static Account fetchRuneScapeAcc() throws IOException {
        return fetchAccount(ACCOUNT_SERVER_URL + "account/osrs");
    }

    private static Account fetchAccount(String urlText) throws IOException {
        System.out.println(urlText);
        URL url = new URL(urlText);
        URLConnection conn = url.openConnection();
        BufferedReader connStream = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String response = connStream.readLine();

        JsonParser parser = new JsonParser();
        JsonObject account = parser.parse(response).getAsJsonObject();
        System.out.println(response);
        return new Account(account.get("id"), account.get("login_name"), account.get("password"));
    }

    public static void setStatusFinished(Account twitchAccount, Account amazonAccount, Account rsAccount) throws IOException {
        setStatusFinished(ACCOUNT_SERVER_URL + "status/name/" + twitchAccount.username + "/finished");
        setStatusFinished(ACCOUNT_SERVER_URL + "status/name/" + amazonAccount.email + "/finished");
        setStatusFinished(ACCOUNT_SERVER_URL + "status/" + rsAccount.id + "/finished");
    }

    public static void setStatusFailed(Account twitchAccount, Account amazonAccount, Account rsAccount) throws IOException {
        if (!twitchAccount.username.equals("")) {
            setStatusFinished(ACCOUNT_SERVER_URL + "status/name/" + twitchAccount.username + "/error");
        }

        if (!amazonAccount.email.equals("")) {
            setStatusFinished(ACCOUNT_SERVER_URL + "status/name/" + amazonAccount.email + "/error");
        }

        if (rsAccount == null) {
            System.out.println("No RuneScape acc loaded");
        } else if (!rsAccount.id.isEmpty()) {
            setStatusFinished(ACCOUNT_SERVER_URL + "status/" + rsAccount.id + "/error");
        }
    }

    private static void setStatusFinished(String urlText) throws IOException {
        URL url = new URL(urlText);
        URLConnection conn = url.openConnection();
        BufferedReader connStream = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String response = connStream.readLine();
        System.out.println(response);
    }

    public static void executePost(String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            // Create connection
            url = new URL(ACCOUNT_SERVER_URL + "account");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static boolean waitForUrl(WebDriver driver, String urlText, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.urlContains(urlText));
        return driver.getCurrentUrl().contains(urlText);
    }

    public static WebElement waitForElement(WebDriver driver, By element, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
        return driver.findElement(element);
    }

    public static int getRandom(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static String getLastWindow(WebDriver driver) {
        Set<String> allWindows = driver.getWindowHandles();

        if (OpenedWindows.size() > 1) {
            OpenedWindows.forEach((window) -> {
                OpenedWindows.remove(window);
            });
        }
        // ostane samo še na novo odprto okno
        String lastWindow = allWindows.iterator().next();
        OpenedWindows.add(lastWindow);

        return lastWindow;
    }

    public static boolean waitIsElementPresent(WebDriver driver, int timeout, By element) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(element));
            return true;
        } catch (TimeoutException ex) {
            return false;
        }
    }

    public static void sleeping(int timer) {
        try {
            Thread.sleep(timer);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
