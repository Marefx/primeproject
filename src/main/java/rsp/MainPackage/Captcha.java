package rsp.MainPackage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import rsp.MainPackage.Models.CaptchaResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Captcha {
    public static void solveCaptcha(WebDriver driver) throws Exception {
        driver.switchTo().defaultContent();
        String token = getCaptchaToken();
        ((JavascriptExecutor) driver).executeScript("___grecaptcha_cfg.clients[0].aa.l.callback('" + token + "')");
    }

    private static String getCaptchaToken() throws Exception {
        int numOfTries = 0;
        int maxNumOfTries = 20;
        CaptchaResponse captchaID = new CaptchaResponse(getCaptchaID());
        CaptchaResponse token = new CaptchaResponse();

        System.out.println(captchaID);
        if (captchaID.OK) {
            do {
                token = new CaptchaResponse(getCaptchaToken(captchaID.value));
                if (!token.OK) {
                    System.out.println("Waiting for 2captcha response...");
                    Helpers.sleeping(5000);
                }
                numOfTries++;
            } while (!token.OK || numOfTries <= maxNumOfTries);
            System.out.print(token);
        }

        if (!token.OK) {
            throw new Exception("Failed to solve captcha!");
        }

        return token.value;
    }

    private static String getCaptchaID() throws IOException {

        URL url = new URL("http://2captcha.com/in.php");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

        writer.append("key=ddf8f60f8802535f947a699f6006247d"
                + "&method=userrecaptcha"
                + "&googlekey=6Ld65QcTAAAAAMBbAE8dkJq4Wi4CsJy7flvKhYqX"
                + "&pageurl=https://www.twitch.tv/");
        writer.flush();

        BufferedReader responseReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String response = responseReader.readLine();
        conn.disconnect();
        return response;
    }

    private static String getCaptchaToken(String id) throws IOException {
        URL url = new URL("http://2captcha.com/res.php?key=ddf8f60f8802535f947a699f6006247d"
                + "&action=get"
                + "&id=" + id);
        URLConnection conn = url.openConnection();
        BufferedReader responseReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        return responseReader.readLine();
    }
}
