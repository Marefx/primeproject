package rsp.MainPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import rsp.MainPackage.Models.Account;

public class Twitch {
    public static boolean twitchRegister(WebDriver driver, Account twitchAccount) throws Exception {
        driver.get(Helpers.TWITCH_URL);
        Helpers.sleeping(500); // včasih ne klikne logina

        WebElement signUpButton = Helpers.waitForElement(driver, By.xpath("//*[@id=\"root\"]/div/div[2]/nav/div/div[5]/div/div[2]/button"), 20);
        signUpButton.click();
        Helpers.sleeping(250);

        WebElement username = Helpers.waitForElement(driver, By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[1]/div/div[2]/input"), 20);
        WebElement password = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[2]/div/div[2]/div[1]/input"));
        WebElement email = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[4]/div/div[2]/input"));

        username.sendKeys(twitchAccount.username);
        Helpers.sleeping(250);
        password.sendKeys(twitchAccount.password);
        Helpers.sleeping(250);
        email.sendKeys(twitchAccount.email);
        Helpers.sleeping(250);

        WebElement month = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[3]/div/div[2]/div[1]/select"));
        month.click();
        WebElement monthOption = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[3]/div/div[2]/div[1]/select/option[" + Helpers.getRandom(2, 10) + "]"));
        monthOption.click();
        WebElement day = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[3]/div/div[2]/div[2]/div/input"));
        day.sendKeys(String.valueOf(Helpers.getRandom(5, 25)));
        WebElement year = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[3]/div/div[2]/div[3]/div/input"));
        year.sendKeys(String.valueOf(Helpers.getRandom(1980, 1995)));

        Captcha.solveCaptcha(driver);

        username.submit();
        Helpers.sleeping(6000);
        driver.get(Helpers.TWITCH_URL);
        Helpers.sleeping(500);

        return true;
    }

    public static void twitchLogin(WebDriver driver, Account twitchAccount) throws Exception {
        driver.get(Helpers.TWITCH_URL);

        Helpers.sleeping(500); // včasih ne klikne logina

        WebElement LoginButton = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/nav/div/div[5]/div/div[1]/button"));
        LoginButton.click();

        WebElement Username = Helpers.waitForElement(driver, By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[1]/div/div[2]/input"), 20);
        WebElement Password = driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[1]/form/div/div[2]/div/div[1]/div[2]/input"));
        Username.sendKeys(twitchAccount.username);
        Helpers.sleeping(250);
        Password.sendKeys(twitchAccount.password);
        Helpers.sleeping(250);
        Username.submit();

        Captcha.solveCaptcha(driver);

        WebElement ContinueLogin = Helpers.waitForElement(driver, By.xpath("/html/body/div[2]/div/div/div/div[1]/div[3]/div/button"), 20);
        ContinueLogin.click();
        Helpers.sleeping(5000);// da se vse naloži
    }
}
